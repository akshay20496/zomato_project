# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
import seaborn as sns
import warnings
warnings.filterwarnings('ignore')

df = pd.read_csv(r'zomato.csv')

df.shape

df.columns.tolist()

df.head()

df.isnull().any(axis=0)

df.isnull().sum()

#dropping all null values in location column
df.dropna(axis = 0, subset = ['location'], inplace = True)
# df['location'].unique()
# len(df['location'].unique())

#making new dataframe
locations = pd.DataFrame()
#adding name column to the locations
locations['Name'] = df['location'].unique()

#Extract longitude and latitude for each and every location

!pip install geopy
from geopy.geocoders import Nominatim

geolocator = Nominatim(user_agent = 'app')

#defining new lists
lat = []            #for latitude
lon = []            #for logitude

for location in locations['Name']:
    #converting this location into geographical format because right now it is in string format
    location = geolocator.geocode(location)
    if location is None:
        lat.append(np.nan)
        lon.append(np.nan)
    else:
        lat.append(location.latitude)
        lon.append(location.longitude)

#creating new columns in locations dataframe and assign the latitude and longitude values
locations['lat'] = lat
locations['lon'] = lon

#creating a csv file
locations.to_csv('zomato_locations.csv', index = False)

#creating a new dataframe and two columns of location name and their value counts
rest_locations = df['location'].value_counts().reset_index()
rest_locations.columns = ['Name','count']


#now mwrging the rest_locations and locations dataframes 
Restaurant_locations = rest_locations.merge(locations, on = 'Name', how = 'left').dropna()

#generating the basemap of Bengaluru city 
!pip install folium
#creating a function to generate a basemap
def generatebasemap(default_location=[12.97,77.59],default_zoom_start=12):
    basemap = folium.Map(location = default_location, zoom_start = default_zoom_start)
    return basemap

import folium
basemap = generatebasemap()basemap.save('plot_map.html')


#plot heatmap for the restaurants in Bengaluru
from folium.plugins import HeatMap
x = HeatMap(Restaurant_locations[['lat','lon','count']], zoom = 20).add_to(basemap)
x.save('restaurants_heatmap.html')

#generate marker cluster analyis of restaurants
from folium.plugins import FastMarkerCluster
y = FastMarkerCluster(Restaurant_locations[['lat','lon','count']],zoom = 20).add_to(basemap)
y.save('restaurants_marker_cluster.html')

#restaurants of high average ratings
df['rate'].unique()
df.dropna(axis = 0, subset = ['rate'], inplace = True)

#creating the function to get only rating but not this /5
def split(a):
    return a.split('/')[0]

df['rating'] = df['rate'].apply(split)    #creating column of rating in dataframe

#replacing NEW and - with 0 in dataframe
df['rating'].unique()
df.replace('NEW',0,inplace=True)
df.replace('-',0,inplace=True)

df.dtypes
#rating column is an object type
#converting rating column into numeric type
df['rating'] = pd.to_numeric(df['rating'])  

#getting the average value of rating for each and every location
df.groupby('location')['rating'].mean().sort_values(ascending=False)
#storing ratings in avg_rating and their locations in location
avg_rating = df.groupby('location')['rating'].mean().sort_values(ascending=False).values
location = df.groupby('location')['rating'].mean().sort_values(ascending=False).index

#creating new dataframe
rating = pd.DataFrame()

#defining new lists
lat = []            #for latitude
lon = []            #for logitude

for loc in location:
    #converting this location into geographical format because right now it is in string format
    loc = geolocator.geocode(loc)
    if loc is None:
        lat.append(np.nan)
        lon.append(np.nan)
    else:
        lat.append(loc.latitude)
        lon.append(loc.longitude)

rating['location'] = location
rating['lat'] = lat
rating['lon'] = lon
rating['avg_rating'] = avg_rating

#removing null values
rating.isnull().sum()
rating.dropna(inplace = True)

#heatmap of restaurants rating
z = HeatMap(rating[['lat','lon','avg_rating']]).add_to(basemap)
z.save('restaurants_rating_heatmap.html')

#north indian food restaurants
df2 = df[df['cuisines'] == 'North Indian']

north_india = df2.groupby('location')['url'].count().reset_index()
north_india.columns = ['Name','count']

z_locations = pd.read_csv(r'zomato_locations.csv')

#merging the north_india and z_locations dataframes

north_india = north_india.merge(z_locations, on = 'Name', how = 'left').dropna()

#generating basemap
def generatebasemap(default_location=[12.97,77.59],default_zoom_start=12):
    basemap = folium.Map(location = default_location, zoom_start = default_zoom_start)
    return basemap

basemap = generatebasemap()

#generating heatmap for north india restaurants
p = HeatMap(north_india[['lat','lon','count']], zoom = 20, radius = 15).add_to(basemap)
p.save('north_indian.html')

#automate the spatial analysis
#creating function to get heatmap for every zone in the data
df['cuisines'].unique()
df['cuisines'].value_counts()
def heatmap_zone(zone):
    df2 = df[df['cuisines'] == zone]
    df_zone = df2.groupby('location')['url'].count().reset_index()
    df_zone.columns = ['Name','count']
    df_zone = df_zone.merge(z_locations, on = 'Name', how = 'left').dropna()
    q = HeatMap(df_zone[['lat','lon','count']], zoom = 20, radius = 15).add_to(basemap)
    return q

heatmap_zone('South Indian')
heatmap_zone('South Indian').save('south_indian.html')

heatmap_zone('Biryani').save('biryani.html')











